import { Form, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

/*
	Mini-Activity - add input fields for firstName, lastName, and mobileNo.
*/

// Define state hooks for all input fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {

	// Allows us to consume the User Context/Data and its properties for validation
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// create state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// create a state to determine whether the submit button is enabled or not
	const [isActive, setisActive] = useState('');

	console.log(firstName);
	console.log(lastName);
	console.log(mobileNo);
	console.log(email);
	console.log(password1);
	console.log(password2);


	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: "POST",
      headers:{
        'Content-Type':'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {
    	console.log(data);

    	if(data === true) {
    		Swal.fire({
          title: "Duplicate Email Found!",
          icon: "error",
          text: "Kindly provide another email to complete the registration!"
        });
    	} else {

    		fetch('http://localhost:4000/users/register',{
          method:"POST",
          headers:{
            'Content-Type':'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            mobileNo: mobileNo,
            password: password1
          })
        })
        .then(res => res.json())
        .then(data => {

        	if(data === true) {
        		setFirstName("");
						setLastName("");
						setMobileNo("");
						setEmail("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
		          title: "Registration successful!",
		          icon: "success",
		          text: "Welcome to Zuitt!"
		        });

		        navigate("/login")

        	} else {
        		Swal.fire({
		          title: "Something went wrong!",
		          icon: "error",
		          text: "Please try again!"
		        });
        	}
        })
    	}
    })
		// alert("Thank you for registering!");
	}

	/*
		Two Way Binding
		- It is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so what we dont have to save it just before we submit

		e.target - current element where the events happens
		e.target.value - current value of the elements where the event happened

	*/

useEffect(() => {

	if ((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
		setisActive(true)
	} else {
		setisActive(false)
	}

}, [firstName, lastName, mobileNo, email, password1, password2])

  return (
  	(user.id !== null)
    ?
    <Navigate to="/courses" />
    :
    <Form onSubmit={(e) => registerUser(e)}>
    <h1>Register</h1>
    	<Row>
	      <Form.Group as={Col} md="4" className="mb-3" controlId="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control 
	        type="text" 
	        placeholder="First Name" 
	        value = {firstName}
	        onChange = {e => setFirstName(e.target.value)}
	        required
	        />
	      </Form.Group>

	      <Form.Group as={Col} md="4" className="mb-3" controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control 
	        type="text" 
	        placeholder="Last Name" 
	        value = {lastName}
	        onChange = {e => setLastName(e.target.value)}
	        required
	        />
	      </Form.Group>

	      <Form.Group as={Col} md="8" className="mb-3" controlId="mobileNo">
			<Form.Label>Mobile No</Form.Label>
	        <Form.Control 
	        type="text" 
	        placeholder="Mobile No." 
	        value = {mobileNo}
	        onChange = {e => setMobileNo(e.target.value)}
	        required
	        />
	     </Form.Group>

	     <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        type="email" 
	        placeholder="Enter email" 
	        value = {email}
	        onChange = {e => setEmail(e.target.value)}
	        required
	        />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	     </Form.Group>

	     <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        type="password" 
	        placeholder="Password" 
	        value = {password1}
	        onChange = {e => setPassword1(e.target.value)}
	        required
	        />
	     </Form.Group>

	     <Form.Group className="mb-3" controlId="password2">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        type="password" 
	        placeholder="Verify Password" 
	        value = {password2}
	        onChange = {e => setPassword2(e.target.value)}
	        required
	        />
	     </Form.Group>
	    </Row>

	    { isActive ?
		    <Button variant="success" type="submit" id="submitBtn">
		    	Submit
		    </Button>
		    :
		    <Button variant="danger" type="submit" id="submitBtn" disabled>
		    	Submit
		    </Button>
		}
    </Form>
  );
}
