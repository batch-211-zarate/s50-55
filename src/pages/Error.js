import Banner from '../components/Banner';


export default function Error() {

	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return (
		// "prop" name is up to developer. 
		<Banner bannerProp ={data} />
		
	);
}

	// <Row className="errorContainer">
	// 	<Col>
	//         <h1>404 Error - Page Not Found</h1>
	//         <p>The page you are looking for cannot be found</p>
	//         <Button as={Link} to="/" variant="primary" id="homeBtn">
	//       	Back to Home
	//     	</Button>
	// 	</Col>
	// </Row> 