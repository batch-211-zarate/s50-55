import React from 'react';

// Creation of Context Object

const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed.
export const UserProvider = UserContext.Provider;

export default UserContext;