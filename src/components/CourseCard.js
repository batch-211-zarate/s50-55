// import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// console.log(props);


	// useState hook:
	// A hook in React is a kind of tool. The useState hook allows creation and manipulation of states

	// States are a way for React to keep track of any value and associate it with a component

	// When a state changes, React re-renders ONLY the specific component or part of the component that changed (and not the entire page or components whose states have not changed)

	
	// state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed.
	// state setter: State setters are the ONLY way to change a state's value. By convention, they are named after state.
	// default state: The state's initial value on "component mount"
	// Before component mounts, a state actually defaults to undefined, then is changed to its default state

		// Syntax:
			/*
				const [state, setState] = useState(default state);
			*/

	// Array Destructuring to get the state and the setter
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(10);

	let { name, description, price, _id } = courseProp;

	// Refactoriing the enroll function and instead use "useEffect" hooks
	// function enroll() {
	// 	if(count !== 10) {
	// 		setCount(count + 1);
	// 		setSeats(seats - 1);
	// 	}
	// }

	// Apply the use effect hook
	// useEffect makes any given code block happen when a state changes AND when a component first mounts (such as on initial page load)

		// Syntax
			// useEffect(function, [dependencies])

	// useEffect(() => {
	// 	if (seats === 0) {
	// 		alert("No more seats available");
	// 	}
	// }, [count, seats])

	
	/*
		Activity:
		1. Create a state hook that will represent the number of available seats for each course

		2. It should default to 10, and decrement by 1 each time a student enrolls

		3. Add a condition that will show an alert that no more seats are available if the seat states is 0.

	*/

	return (
		<Row className = "mt-3 mb-3">
			<Col>
				<Card className="cardCourseCard p-3">
					<Card.Body className="card-purple">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						<Button as={Link} to={`/courses/${_id}`}>Details</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
} 